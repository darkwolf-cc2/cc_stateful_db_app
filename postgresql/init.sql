/-- TODO: integreate this into docs

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO democlient;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO democlient;

CREATE TABLE IF NOT EXISTS user_info (
  id  SERIAL,
  username varchar(100) NOT NULL UNIQUE,
  password varchar(450) NOT NULL,
  fullname varchar(100) NOT NULL UNIQUE
);
