# Stateful App
This is a demo rest API webserver to be deployed to kubernetes written in python.
Additional details on running and deploying the cluster are in the [API/README.md](API/README.md)

## Sources
Stateful demo application written in python based on:
https://kubernetes.io/docs/tasks/run-application/run-replicated-stateful-application/

Vault Config basis:
https://learn.hashicorp.com/tutorials/vault/agent-kubernetes?in=vault/kubernetes
https://learn.hashicorp.com/vault/identity-access-management/vault-agent-k8s
