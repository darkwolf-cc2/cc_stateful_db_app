#!/usr/bin/env Python
# stdlib
import json
import os
from typing import List

# 3rd party
import uvicorn
from sqlalchemy.orm import Session
from fastapi import Depends, FastAPI, HTTPException

# this package
import database
import models
import schemas
import crud


VAULT_SECRETS_FILE = '/etc/secrets/settings.json'
if os.path.isfile(VAULT_SECRETS_FILE):
    settings = json.load(open(VAULT_SECRETS_FILE))
else:
    print("WARNING: did not find the vault secret file. defaulting to the develepment settings.json")
    settings = json.load(open('settings.json'))

db_uri = f'{settings["database"]["connection"]}{settings["database"]["username"]}:{settings["database"]["password"]}@{settings["database"]["db_host"]}/{settings["database"]["db_name"]}'
engine, localsession = database.db_engine(db_uri)

app = FastAPI()


def get_db():
    db = None
    try:
        db = localsession()
        yield db
    finally:
        db.close()


@app.get("/")
async def root():
    return {"message": "API is running."}


@app.post("/user", response_model=schemas.UserInfo)
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_username(db, username=user.username)
    if db_user:
        raise HTTPException(status_code=400, detail="Username already registered")
    return crud.create_user(db=db, user=user)


@app.get("/user", response_model=schemas.UserInfo)
def get_user(username: str, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_username(db, username=username)
    if not db_user:
        raise HTTPException(status_code=400, detail="No user found with username {user.name}")
    return db_user


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8080)
