# Python Sample App

## Overview
This folder contains code for a sample Python3 FastAPI app. When run, this app hosts a REST API which can be queried to create a user and to read a user's info from the database.

This app works with different backends and is configured through a `settings.json` file in the same directory as `app.py`. File format described below.

``` json
{
    "database":{
        "connection": "postgresql://",
        "db_name": "database"
        "db_host": "localhost:5432",
        "username": "docker",
        "password": "docker",
    }
}
```



## Vault setup

Some commands done on vault to enable the service account to login to vault.
This was adapted from the hashicorp tutorial on [injecting secrets into Kubernetes](https://www.hashicorp.com/blog/injecting-vault-secrets-into-kubernetes-pods-via-a-sidecar)

``` bash
# exec into the container
$ kubectl exec -it vault-0 -n default -- sh

# Configure kubernetes auth if not already configured
$ vault write auth/kubernetes/config \
   token_reviewer_jwt="$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" \
   kubernetes_host=https://${KUBERNETES_PORT_443_TCP_ADDR}:443 \
   kubernetes_ca_cert=@/var/run/secrets/kubernetes.io/serviceaccount/ca.crt

# Create a role for the app to use
$ vault write auth/kubernetes/role/stateful-app-system-vault-agent \
    bound_service_account_names=stateful-app-vault-auth \
    bound_service_account_namespaces=stateful-app-system \
    policies=database-config \
    ttl=24h
```

## Postgres Setup
Run the steps in the [postgres init script](../postgresql/init.sql)

```bash
$ kubectl run demo-psql-postgresql-client --rm  -it --restart='Never' \
    --namespace default --image docker.io/bitnami/postgresql:11.11.0-debian-10-r31 \
    --env="PGPASSWORD=$POSTGRES_PASSWORD" --command \
    -- psql --host demo-psql-postgresql -U postgres -d postgres -p 5432 -c "$(cat ../postgresql/init.sql)"
```


## Deploy

You'll need to first create a registry secret in the `stateful-app-system` namespace to be used for pulling images from gitlab.
A sample command for this is below

``` bash
# Create namespace if not already created
$ kubectl create namespace stateful-app-system || true

# Create the docker registry secret to access the gitlab regitry
$ kubectl create secret docker-registry registry-credentials -n stateful-app-system \
    --docker-server registry.gitlab.com \
    --docker-username <username> \
    --docker-password <access token with read/write registry access>
```

After creating the pull secret, you can deploy with
``` bash
$ cd deployment

# This is an example image from a merge request
$ DEPLOY_IMAGE=registry.gitlab.com/darkwolf-cc2/cc_stateful_db_app/postgresql-backend:latest

# This is the command to edit the kustomize files with the new image and deploy this
$ kustomize edit set image image="${DEPLOY_IMAGE}" && kustomize build . | kubectl apply -f -
```

To patch for updating the secret
``` bash
$ kubectl patch deployment stateful-app-deployment -n stateful-app-system --patch "$(cat patch/patch.yaml)"
```

## Querying the app
Example commands for querying the app are below

``` bash
# After deploying the application
$ kubectl port-forward service/stateful-app-service -n stateful-app-system 8080:8080 &

# Create user
$ curl -H "Content-type: application/json" -X POST -d '{"username": "robot", "fullname": "super robot", "password": "none"}' 'localhost:8080/user'

# Query for the user
$ curl -H "Content-type: application/json" -X GET 'localhost:8080/user?username=robot
```
